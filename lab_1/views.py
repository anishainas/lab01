from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Anisha Inas Izdihar' # TODO Implement this
mhs_birthyear = 1999

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(mhs_birthyear)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    age = datetime.now().year - birth_year
    return age
